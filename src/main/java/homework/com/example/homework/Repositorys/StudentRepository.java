package homework.com.example.homework.Repositorys;


import homework.com.example.homework.Models.StudentModel;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {
    List<StudentModel> stuM = new ArrayList<StudentModel>();

    public List<StudentModel> findAllstu(){
        stuM.add(new StudentModel("viseth",1,"A"));
        stuM.add(new StudentModel("Kaka",2,"A"));
        return stuM;
    }
}
