package homework.com.example.homework.Services;

import homework.com.example.homework.Models.StudentModel;
import homework.com.example.homework.Repositorys.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImp implements StudentService{

    private StudentRepository studentRepository;

    @Autowired
    public void setStudentRepository(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override
    public List<StudentModel> findAllstu() {

        return studentRepository.findAllstu();
    }
}
