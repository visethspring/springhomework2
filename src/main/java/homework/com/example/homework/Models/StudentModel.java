package homework.com.example.homework.Models;

public class StudentModel {

    private String name;
    private int id;
    private String grade;


    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void Display(){
        System.out.println("Student is called");
    }
    public StudentModel(String name , int id , String grade){
        this.name = name;
        this.id = id;
        this.grade = grade;
    }

}
